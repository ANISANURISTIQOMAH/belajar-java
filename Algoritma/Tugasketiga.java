import java.util.Scanner;

public class Tugasketiga {
    public static void main(String[] args) {

       //Implementasikan algoritma linear search dalam code java

        Scanner input = new Scanner(System.in);
        //deklarasi variable
        int arr[] = { 2, 3, 4, 10, 40 };
        int N = arr.length;
        int i,key;

        //input nilai array yang di cari
        System.out.print("Input nilai yang dicari : ");
        key = input.nextInt();

        //pencarian array
        for (i = 0; i < N; i++) {
            if (arr[i] == key){
                System.out.print("ketemu pada index ke-"+i);
                break;
            }
        }
        if(i == N){
            System.out.print("tidak ketemu");
        }

        System.out.println();

    }
}

