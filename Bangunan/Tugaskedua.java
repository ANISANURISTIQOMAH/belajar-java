import java.util.*;

public class Tugaskedua {
    public static void main(String[] args) {
        Scanner Input = new Scanner(System.in);
        String pilihanUser;
        //pilihan_user
        System.out.println("yuk dipilih ");
        System.out.println("1. luas Lingkaran ");
        System.out.println("2. volume Balok ");
        System.out.println("3. syarat Kawahedukasi");
        //print_pilihan_user

        System.out.print(" Pilihan Anda: ");
        pilihanUser = Input.nextLine();

        switch (pilihanUser) {
            case "1":
                //luas lingkaran
                System.out.println(" selamat datang di luas lingkaran");

                Integer r;
                double Luas;
                //deklarasi
                System.out.print("Jari-Jari: ");
                r = Input.nextInt();
                //proses
                Luas = Math.PI * Math.pow(r, 2);
                //output
                System.out.println("Luas: " + Luas);
                break;
            case "2":
                //volume balok
                System.out.println(" selamat datang di volume balok ");
                //deklarasi
                int panjang, lebar, tinggi, hasil;

                System.out.print("Masukan Panjang Balok: ");
                panjang = Input.nextInt();
                System.out.print("Masukan Lebar balok: ");
                lebar = Input.nextInt();
                System.out.print("Masukan Tinggi Balok: ");
                tinggi = Input.nextInt();

                //proses
                hasil = panjang * lebar * tinggi;
                //output
                System.out.println("Volume Balok tersebut adalah: " + hasil);
                break;
            case "3":
                System.out.println(" selamat datang di syarat kawahedukasi ");
                //umur 17-27
                //pendidikan SMA,SMK,D3,D4,S1
                //memiliki kemauan belajar bersungguh-sungguh
                //bersedia kerja di it consultan

                // Meminta input nama dari user
                System.out.print("Masukkan nama: ");
                String nama = Input.nextLine();

                // Meminta input usia dari user
                System.out.print("Masukkan usia: ");
                int usia = Input.nextInt();
                Input.nextLine();

                // Meminta input pendidikan dari user
                System.out.print("Masukkan pendidikan: ");
                String pendidikan = Input.nextLine();

                // Meminta input memiliki kemauan belajar bersungguh-sungguh
                System.out.print("Memiliki kemauan belajar bersungguh-sungguh (ya/tidak)? ");
                String kemauanBelajar = Input.nextLine();

                // Meminta input bersedia bekerja di IT consultant dari user
                System.out.print("Bersedia bekerja di IT consultant (ya/tidak)? ");
                String bersedia = Input.nextLine();

                // Validasi usia
                boolean hasilUsia = usia >= 17 && usia <= 27;
                System.out.println(hasilUsia);

                //Validasi pendidikan
                boolean hasilPendidikan = pendidikan.equals("SMA") || pendidikan.equals("SMK")
                        || pendidikan.equals("D1") || pendidikan.equals("D3") || pendidikan.equals("D4")
                        || pendidikan.equals("S1");
                System.out.println(hasilPendidikan);

                //validasi bersedia kerja di it consultan
                boolean hasilKemauanBelajar = kemauanBelajar.equals("ya");
                System.out.println(hasilKemauanBelajar);

                //validasi bersedia kerja di it consultan
                boolean hasilBersedia = bersedia.equals("ya");
                System.out.println(hasilBersedia);

                //validasi keempat variabel
                boolean hasilSyarat = hasilUsia == true && hasilPendidikan == true &&
                        hasilKemauanBelajar == true && hasilBersedia == true;
                System.out.println("Selamat anda boleh mendaftar atau bernilai " + hasilSyarat);
                    break;
                }

    }

}

